---
layout: page
title: Publications

permalink: /publications/
---

# Conferences 
{% for publication in site.publications reversed %}
- [**{{ publication.title }}**]({{ publication.website}}) ({{ publication.authors }}) [{{publication.conference}}]<br/>
{% endfor %}

# Theses
{% for thesis in site.theses reversed %}
- [**{{ thesis.title }}**]({{ thesis.website}}) ({{ thesis.authors }}) [{{thesis.year}}]<br/>
{% endfor %}

# Preprints
{% for preprint in site.preprints reversed %}
- [**{{ preprint.title }}**]({{ preprint.website}}) ({{ preprint.authors }}) [{{preprint.conference}}]<br/>
{% endfor %}




