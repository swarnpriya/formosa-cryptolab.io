---
layout: default

title: Jasmin compiler in the opam repository
date: 2022-06-28

summary: >-
  The Jasmin compiler can be installed using the opam package manager.
linkback: >-
  Learn how.
---

# Opam repository now featuring Jasmin compiler

The Jasmin compiler is now part of the [opam repository](https://opam.ocaml.org/packages/jasmin/).

Once you have a working `opam`, just run `opam install jasmin` to get the Jasmin compiler installed.
Note that this only installs the compiler (including termination & safety checker, constant-time verifier, reference interpreter…) and no EasyCrypt library nor Coq proofs.

The Jasmin compiler remains available in [nixpkgs](https://search.nixos.org/packages?query=jasmin-compiler).
