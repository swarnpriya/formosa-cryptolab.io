---
layout: project

project: EasyCrypt
website: https://easycrypt.info
git: https://github.com/EasyCrypt/easycrypt.git

short: >-
  EasyCrypt is a toolset for reasoning about relational properties of
  probabilistic computations with adversarial code. Its main application is the
  construction and verification of game-based cryptographic proofs.
---
