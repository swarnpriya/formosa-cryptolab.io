---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: default
---

The Formosa Crypto project federates multiple projects in machine-checked
cryptography and high-assurance cryptographic engineering under a single
banner, to better support developers and users.

Join us on [Zulip](https://formosa-crypto.zulipchat.com).

{% if site.news != empty %}
[Formosa News]({% link news.markdown %})
====

{% assign news = site.news | sort: 'date' | reverse %}
{% for item in news %}
- [**{{ item.title }}**]({{ item.url | relative_url }}) ({{ item.date | date: '%B' }} {{ item.date | date : '%d' | plus:'0' }}{{ item.date | date : ', %Y' }})<br/>
  {% capture link %} [{{ item.linkback }}]({{ item.url | relative_url }}){% endcapture %}
  {{ item.summary | append:link | markdownify | strip_newlines }}
{% endfor %}
{% endif %}

[Formosa Projects]({% link projects.markdown %})
====

{% for project in site.projects %}
- [**{{ project.project }}**]({{ project.url | relative_url }}) — [Project Website]({{ project.website }}) — [Project Repository]({{ project.git }})<br/>
  {{ project.short | markdownify }}
{% endfor %}

[Formosa People]({% link people.markdown %})
====

{% for person in site.people %}
- [**{{ person.name }}**]({{ person.website }}) ({{ person.institution }})
{% endfor %}

[Formosa Affiliations and Supporters]({% link support.markdown %})
====

Formosa and its component projects are supported by a variety of funders and
institutions. Our active members work for the following institutions.<br/>

{%- for supporter in site.institutions -%}
[![The {{ supporter.name }} logo](/assets/logos/{{ supporter.logo }}){: width="30%" style="margin: 10px" }]({{ supporter.website }})
{%- endfor -%}<br/>

In addition, other institutions support, and have supported Formosa or its
constituent projects financially.
We gratefully [acknowledge their support]({% link support.markdown %}).
