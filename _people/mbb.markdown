---
name: Manuel Barbosa
institution: Universidade do Porto & HASLab/INESC TEC
website: https://www.dcc.fc.up.pt/~mbb/

projects: EasyCrypt, Jasmin, Libjade
---
