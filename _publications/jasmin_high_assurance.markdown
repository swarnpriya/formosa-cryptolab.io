---
layout: default
title: "Jasmin: High-assurance and high-speed cryptography"
year: 2017
authors: José Bacelar Almeida, Manuel Barbosa, Gilles Barthe, Arthur Blot, Benjamin Grégoire, Vincent Laporte, Tiago Oliveira, Hugo Pacheco, Benedikt Schmidt, and Pierre-Yves Strub
website: https://hal.archives-ouvertes.fr/hal-01649140
conference: CCS, 2017

---