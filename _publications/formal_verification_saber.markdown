---
layout: default
title: Formal Verification of Saber's Public-Key Encryption Scheme in EasyCrypt
year: 2022
authors: Andreas Hülsing, Matthias Meijers, and Pierre-Yves Strub
website: https://eprint.iacr.org/2022/351
conference: CRYPTO, 2022

---